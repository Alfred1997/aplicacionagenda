package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class ListaActivity extends AppCompatActivity implements Filterable {

    TableLayout tblLista;
    ArrayList<Contacto> contactos;
    Button btnNuevo;
    SearchView srcLista;
    private List<Contacto> ejemploListaLlena;
    TableRow tblListar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
 

        tblLista = (TableLayout) findViewById(R.id.tblLista);
        srcLista = (SearchView) findViewById(R.id.menu_search);
        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        tblListar = (TableRow) findViewById(R.id.tblRow);

        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);

        SearchView searchView = (SearchView) menuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                for (int x = 0; x < contactos.size(); x++) {
                    final Contacto c = new Contacto(contactos.get(x));
                    if(s.toLowerCase().contains(c.getNombre().toLowerCase())){
                        final TableRow nRow = new TableRow(ListaActivity.this);
                        final TextView nText = new TextView(ListaActivity.this);
                        nText.setText(c.getNombre());
                        nText.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
                        nText.setTextColor((c.isFavorite()) ? Color.BLUE : Color.BLACK);
                        nRow.addView(nText);

                        final Button nButton = new Button(ListaActivity.this);
                        nButton.setText(R.string.accion_ver);
                        nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
                        nButton.setTextColor(Color.BLACK);

                        nButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Contacto c = (Contacto) v.getTag(R.string.contacto_g);
                                Intent i = new Intent();
                                Bundle oBundle = new Bundle();
                                oBundle.putSerializable("contacto", c);
                                oBundle.putInt("index", Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                                i.putExtras(oBundle);
                                setResult(RESULT_OK, i);
                                finish();
                            }
                        });
                        nButton.setTag(R.string.contacto_g, c);
                        nButton.setTag(R.string.contacto_g_index, x);
                        nRow.addView(nButton);
                    }else if(s.matches("")){
                        tblLista.removeAllViewsInLayout();
                        tblLista.addView(tblListar);
                        cargarContactos();
                    }
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public void cargarContactos() {
        for (int x = 0; x < contactos.size(); x++) {
            final Contacto c = new Contacto(contactos.get(x));
            final TableRow nRow = new TableRow(ListaActivity.this);
            final TextView nText = new TextView(ListaActivity.this);
            nText.setText(c.getNombre());
            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nText.setTextColor((c.isFavorite()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            final Button nButton = new Button(ListaActivity.this);
            nButton.setText(R.string.accion_ver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c = (Contacto) v.getTag(R.string.contacto_g);
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", c);
                    oBundle.putInt("index", Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    i.putExtras(oBundle);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });
            nButton.setTag(R.string.contacto_g, c);
            nButton.setTag(R.string.contacto_g_index, x);
            nRow.addView(nButton);
            tblLista.addView(nRow);
        }
    }

    public Filter getFilter(){
        return ejemploFiltro;
    }
    private Filter ejemploFiltro = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Contacto> ListaFiltrada = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                ListaFiltrada.addAll(ejemploListaLlena);
            }
            else{
                String patronFiltro = constraint.toString().toLowerCase().trim();
                for(Contacto item : ejemploListaLlena){
                    if(item.getNombre().toLowerCase().contains(patronFiltro)){
                        ListaFiltrada.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = ListaFiltrada;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            contactos.clear();
            contactos.addAll((List)results.values);
        }
    };
}
